package com.qdu.pokerun.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.qdu.pokerun.PokeRun;
import com.qdu.pokerun.actor.trade.BackButton;

public class MenuScreen implements Screen {

    private final Screen previousScreen;
    protected final Stage mainStage;

    protected BackButton backButton;

    //按钮样式
    Skin skin;

    public MenuScreen(Screen previousScreen) {
        skin = new Skin(Gdx.files.internal("style/uiskin.json"));
        mainStage = new Stage(new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        this.previousScreen = previousScreen;
        createBackButton();
    }

    protected void createBackButton() {
        this.backButton = new BackButton(skin, previousScreen);
//        backButton.setBounds(20, mainStage.getViewport().getWorldHeight() - 120, 100, 100);
        mainStage.addActor(backButton);
    }

    protected void showPreviousScreen() {
        PokeRun.instance.setScreen(previousScreen);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this.mainStage);
        this.backButton.setText(PokeRun.rb_default.format("button.back"));
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(230f / 255f, 230f / 255f, 230f / 255f, 1f);
        mainStage.act();
        mainStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        this.mainStage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.mainStage.dispose();
    }
}
