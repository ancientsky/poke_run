package com.qdu.pokerun.screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.qdu.pokerun.PokeRun;
import com.qdu.pokerun.entity.Player;
import com.qdu.pokerun.service.PlayerService;
import com.qdu.pokerun.service.impl.PlayerServiceImpl;
import com.qdu.pokerun.util.PlayerUtil;
import com.qdu.pokerun.util.SoundUtil;

public class LoginScreen implements Screen {

    private Stage mainStage;

    private Table mainTable;
    private Table formTable;
    private Table buttonTable;

    private TextField playerNameTF;
    private TextField pwdTF;

    private CheckBox remmeCB;

    private ImageTextButton loginButton;
    private ImageTextButton registerButton;
    private ImageTextButton exitButton;

    private ImageButton settingButton;

    private MainScreen mainScreen;
    private SettingScreen settingScreen;

    //整体样式
    Skin skin;
    private Image icon;

    public static LoginScreen instance;
    private PlayerService playerService;

    /**
     * 初始化按钮样式
     */
    private void initGlobalSkin() {
        //使用外部文件中的样式
        skin = new Skin(Gdx.files.internal("style/uiskin.json"));
    }

    public LoginScreen() {
        instance = this;

        mainScreen = new MainScreen();
        settingScreen = new SettingScreen(this);

        initGlobalSkin();

        playerService = new PlayerServiceImpl();
    }

    @Override
    public void show() {
        this.mainStage = new Stage(new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        Gdx.input.setInputProcessor(this.mainStage);

        Label tmp;
        this.icon = new Image(new Texture(Gdx.files.internal("img/icon/Login_icon.png")));
        this.icon.setPosition(80,-50);
        this.mainStage.addActor(icon);
        this.mainTable = new Table();
        this.mainTable.setFillParent(true);

        this.mainStage.addActor(this.mainTable);

        this.mainTable.padLeft(32).padRight(32);
        this.mainTable.row().colspan(2);
        //this.mainTable.add(new Image(new Texture(Gdx.files.internal("img/ui/title/pokeball.png")))).maxSize(96, 96);
        this.mainTable.add(new Image(new Texture(Gdx.files.internal("img/ui/title/title.png")))).maxSize(96 * 3, 96);


        this.mainTable.row().colspan(2);
        this.formTable = new Table();
        this.formTable.row().pad(16);
        tmp = new Label(PokeRun.rb_default.format("label.playername"), skin);
        tmp.setColor(Color.BLACK);
        this.formTable.add(tmp);
        this.playerNameTF = new TextField("", skin);
        this.formTable.add(this.playerNameTF);

        this.formTable.row().pad(16);
        tmp = new Label(PokeRun.rb_default.format("label.pwd"), skin);
        tmp.setColor(Color.BLACK);
        this.formTable.add(tmp);
        this.pwdTF = new TextField("", skin);
        this.pwdTF.setPasswordMode(true);
        this.pwdTF.setPasswordCharacter('*');
        this.formTable.add(this.pwdTF);

        this.formTable.row().padTop(64).colspan(2);
        this.remmeCB = new CheckBox(PokeRun.rb_default.format("label.remme"), skin);
        remmeCB.getLabel().setColor(Color.BLACK);
        //this.formTable.add(this.remmeCB);

        this.mainTable.add(this.formTable);


        this.mainTable.row().colspan(2);
        this.buttonTable = new Table();
        this.buttonTable.row().pad(16);
        this.loginButton = new ImageTextButton(PokeRun.rb_default.format("button.login"), skin);
        this.loginButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtil.playNavigationSound();
                if(playerNameTF.getText().equals("") || pwdTF.getText().equals("")){
                    new Dialog(PokeRun.rb_default.format("dialog.missinginfo.title"), skin)
                            .text(PokeRun.rb_default.format("dialog.missinginfo"))
                            .button(PokeRun.rb_default.format("dialog.confirm"), true)
                            .key(Input.Keys.ENTER, true)
                            .key(Input.Keys.ESCAPE, true)
                            .show(mainStage);
                    return;
                }
                Player player = playerService.checkPlayerPwd(playerNameTF.getText(), pwdTF.getText());
                if (!player.getPlayerName().equals("")) {
                    PokeRun.player = player;
                    PokeRun.instance.setScreen(mainScreen);
                    SoundUtil.ensureMusicPlaying();
                    dispose();
                } else {
                    new Dialog(PokeRun.rb_default.format("dialog.wronginfo.title"), skin)
                            .text(PokeRun.rb_default.format("dialog.wronginfo"))
                            .button(PokeRun.rb_default.format("dialog.confirm"), true)
                            .key(Input.Keys.ENTER, true)
                            .key(Input.Keys.ESCAPE, true)
                            .show(mainStage);
                }
            }
        });

        this.registerButton = new ImageTextButton(PokeRun.rb_default.format("button.register"), skin);
        this.exitButton = new ImageTextButton(PokeRun.rb_default.format("button.exit"), skin);
        this.exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtil.playNavigationSound();
                dispose();
                Gdx.app.exit();
            }
        });

        this.buttonTable.add(this.loginButton).minSize(96, 64);
        this.buttonTable.add(this.registerButton).minSize(96, 64);
        this.buttonTable.add(this.exitButton).minSize(96, 64);
        this.mainTable.add(this.buttonTable);


        this.mainTable.row().colspan(2);
        this.settingButton = new ImageButton(new TextureRegionDrawable(new Texture(Gdx.files.internal("img/ui/profile/setting.png"))));
        this.settingButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtil.playNavigationSound();
                PokeRun.instance.setScreen(settingScreen);
                SoundUtil.ensureMusicPlaying();
//                dispose();
            }
        });
        //this.mainTable.add(this.settingButton).maxSize(48, 48);
        this.settingButton.setSize(48, 48);
        this.settingButton.setPosition(Gdx.graphics.getWidth()-60,Gdx.graphics.getHeight()-60);
        this.mainStage.addActor(this.settingButton);
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(Color.WHITE);
        this.mainStage.act(Gdx.graphics.getDeltaTime());
        this.mainStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        this.mainStage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.mainStage.dispose();
    }
}
