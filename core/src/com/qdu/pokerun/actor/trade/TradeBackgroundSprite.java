package com.qdu.pokerun.actor.trade;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Trade页面背景
 * @author muyoo
 */
public class TradeBackgroundSprite extends Container {
    private Image trade,mc;
    boolean BgType;
    public TradeBackgroundSprite(){
        this.trade = new Image(new Texture(Gdx.files.internal("img/bg/tradeBg.jpg")));
        this.mc = new Image(new Texture(Gdx.files.internal("img/bg/mcBg.jpg")));
        setSize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        setActor(this.trade);
        fill();
        this.BgType = true;
    }

    public void switchBackground (){
        if(this.BgType) {
            setActor(this.mc);
        }else
            setActor(this.trade);

        this.BgType =!this.BgType;
    }
}
