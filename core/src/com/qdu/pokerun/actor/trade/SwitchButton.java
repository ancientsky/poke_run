package com.qdu.pokerun.actor.trade;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class SwitchButton extends ImageTextButton {
    public SwitchButton(Skin skin , TradeBackgroundSprite bg) {
        super("切换", skin);
        getLabel().setSize(15,15);
        setSize(100,60);
        setPosition(Gdx.graphics.getWidth()-getWidth()-30, Gdx.graphics.getHeight()-getHeight()-20);

        addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                bg.switchBackground();
            }
        });
    }
}
