package com.qdu.pokerun.util;

import com.badlogic.gdx.graphics.Color;

public class BackgroundUtil {

    /**
     * 通过背景的ID获取该背景的名称
     *
     * @param bgID
     * @return
     */
    public static String getBgNameByID(int bgID){
        StringBuilder bgName=new StringBuilder();
        switch (bgID){
            case 1:bgName.append("rock");break;
            case 2:bgName.append("water");break;
            default:bgName.append("grass");
        }
        return bgName.toString();
    }

    /**
     * 通过背景的ID获取该背景的填充色
     *
     * @param bgID
     * @return
     */
    public static Color getBgColorByID(int bgID){
        Color bgColor;
        switch (bgID){
            case 1:bgColor=Color.BROWN;break;
            case 2:bgColor=Color.CYAN;break;
            default:bgColor=Color.CHARTREUSE;
        }
        return bgColor;
    }
}
