create schema pokerun CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use pokerun;

create table `player` (
	`uuid` bigint primary key auto_increment,
    -- basic info
    `playerName` varchar(32) not null,
    `upwd` binary(20) not null,
    `email` varchar(255) not null unique,
    `avator` varchar(255),
    -- 对于其他信息，可以之后在建一张表
    -- index
    -- 使用index，可以考虑延迟关联优化查询
    index(`playerName`),
    index(`email`),
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

create table `item` (
	`itemID` bigint primary key auto_increment,
    -- basic info
    `meta` varchar(32) not null,
    `avator` varchar(255),
    -- 对于其他信息，可以之后在建一张表
    -- index
    -- 使用index，可以考虑延迟关联优化查询
    index(`meta`),
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

insert into item (meta,avortor) 
values (
	'blower'
	'烘干精灵身上的毛'
)，
(
	'brush'
	'梳理精灵身上的毛'
)，
(
	'blower'
	'梳理精灵身上的毛'
)，
(
	'comb'
	'梳理精灵身上的毛'
)，
(
	'drug'
	'吃药时间到！！！'
)，
(
	'towel'
	'擦干精灵身上的毛'
)，
(
	'apriblender'
	''
)，
(
	'apriblender_bigger'
	''
)，
(
	'lumiose_galette'
	''
)，
(
	'poffin'
	''
)，
(
	'poke_bean'
	''
)，
(
	'poke_puff'
	''
)，
(
	'porygon_candy'
	''
)，
(
	'rainbow_poke_bean'
	''
)，