# poke_run

#### 介绍
1组期末项目：宝可跑步（PokeRun）
组员：yyx、zts、gyf、wjc
一个用来与pixelmon（Minecraft）联动、计步+的养成轻游戏

#### 软件架构
使用技术：
1.  Android
2.  LibGDX
3.  MySQL

#### 安装教程

1.  将项目clone到IDE内或直接下载到本地并使用IDE导入build.gradle
2.  等待依赖下载并调整完毕
3.  运行gradle的task即可食用

#### 使用说明

暂无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
