package com.qdu.pokerun.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.qdu.pokerun.PokeRun;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title="Poke Run";
		config.height=800;
		config.width=480;
		new LwjglApplication(new PokeRun(), config);
	}
}
